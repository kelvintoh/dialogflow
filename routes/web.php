<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/webhook', 'MessengerController@index');
Route::post('/facebook_messenger_api','DialogflowController@index');
Route::get('/facebook_messenger_api','DialogflowController@index');

Route::get('/', function () {
    return view('welcome');
});
