<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
use Google\Cloud\Dialogflow\V2\Agent;
use Google\Cloud\Dialogflow\V2\AgentsClient;
use Google\Cloud\Dialogflow\V2\DetectIntentRequest;
use Google\Cloud\Dialogflow\V2\QueryInput;
use Google\Cloud\Dialogflow\V2\QueryParameters;
use Google\Cloud\Dialogflow\V2\SessionsClient;
use Google\Cloud\Dialogflow\V2\EntityTypesClient;
use Google\Cloud\Dialogflow\V2\SessionEntityType;
use Google\Cloud\Dialogflow\V2\EntityType_Entity;
use Google\Cloud\Dialogflow\V2\EntityType_Kind;
use Google\Cloud\Dialogflow\V2\TextInput;
use Google\Cloud\Dialogflow\V2\EventInput;
use Google\Cloud\Dialogflow\V2\EntityType;
use Google\Cloud\Dialogflow\V2\Intent_Message;
use Google\Cloud\Dialogflow\V2\Intent\Message;
use Google\Protobuf\internal\OneofField;
use Google\Cloud\Dialogflow\V2\Intent_Message_Platform;
use Google\Cloud\Dialogflow\V2\Intent_Message_Card;
use Google\Cloud\Dialogflow\V2\GetIntentRequest;
use Google\Protobuf\Internal\MapField;
use Google\Protobuf\Value;
use Google\Protobuf\Struct;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Carbon\Carbon;


class DialogflowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

		$this->verifyToken();
        
		$input = json_decode(file_get_contents('php://input'), true);
        $id = $input['entry'][0]['messaging'][0]['sender']['id'];
		$time = Carbon::now('Asia/Hong_Kong')->toDateTimeString();
		$database = DB::table('messenger')->select('*')->where('sender_id','=',$id)->get();
		foreach($database as $da)
		{
			$data = $da->sender_id;
			$status = $da->status;
		}
		$orderlist2 = DB::table('order')->select('order_id')->get();	
		$productlist2 = DB::table('product')->select('product_code','product_name')->get();

        if (isset($input['entry'][0]['messaging'][0]['message']['is_echo'])) {
            exit;
        }
		
		//store id for new user 
		if(empty($data) || $data != $id)
		{
			$database = DB::table('messenger')->insert([
			'sender_id' => $id, 
			'session_id' => '1', 
			'youbeli_id' => '1', 
			'status' => '1', //Bot operational, '1' = operate, '0' = not operate
			'last_receive' => $time]);
			
			$orderid = DB::table('sessions')->insert([
			'sender_id' => $id, 
			'order_id' => '0',
			'product_code' => '0']);
		}
		
		if($status == 1)
		{
			if (isset($input['entry'][0]['messaging'][0]['message']['text'])) 
			{
				$message = $input['entry'][0]['messaging'][0]['message']['text'];
		
				//new EntityTypeClient
				$EntityClient = new EntityTypesClient([
					'credentials' => storage_path('Youbeli-5676ec362f0c.json')
				]);
				$formattedParent = $EntityClient->projectAgentName('youbeli-e34ca');
				
				//Listing all the EntityTypes
				$pagedResponse = $EntityClient->ListEntityTypes($formattedParent);
				foreach($pagedResponse->iterateAllElements() as $element)
				{
					if($element->getDisplayName() == 'Order_Number')
					{
						$EntityName = $element->getName();
						
						//EntityType set Order Entities
						$orderlist = array();
						foreach($orderlist2 as $orderlist1)
						{
							$EntityTypeEntity = new EntityType_Entity();
							$EntityTypeEntity->setValue($orderlist1->order_id);
							$EntityTypeEntity->setSynonyms([$orderlist1->order_id]);
							
							$orderlist[] = $EntityTypeEntity;
							
						}
						//EntityType OrderList
						$EntityType = new EntityType();
						$EntityType->setName($EntityName);
						$EntityType->setDisplayName('Order_Number');
						$EntityType->setKind(1);
						$EntityType->setAutoExpansionMode(0);
						$EntityType->setEntities($orderlist);
						
						$pageResponse = $EntityClient->UpdateEntityType($EntityType);
					}
					
					
					elseif($element->getDisplayName() == 'Product_Name')
					{
						$EntityName = $element->getName();
						
						//EntityType set ProductCode Entities
						$productlist = array();
						foreach($productlist2 as $productlist1)
						{
							$EntityTypeEntity = new EntityType_Entity();
							$EntityTypeEntity->setValue($productlist1->product_name);
							$EntityTypeEntity->setSynonyms([$productlist1->product_name,$productlist1->product_code]);
							
							$productlist[] = $EntityTypeEntity;
							
						}
						//EntityType ProductCodeList
						$EntityType = new EntityType();
						$EntityType->setName($EntityName);
						$EntityType->setDisplayName('Product_Name');
						$EntityType->setKind(1);
						$EntityType->setAutoExpansionMode(0);
						$EntityType->setEntities($productlist);
						
						$pageResponse = $EntityClient->UpdateEntityType($EntityType);
					}
				}
				
				//new SessionClient
				$sessionsClient = new SessionsClient([
					'credentials' => storage_path('Youbeli-5676ec362f0c.json')
				]);
				$formattedSession = $sessionsClient->sessionName('youbeli-e34ca', '123123');
				
				$queryParams = new QueryParameters();
				
				//create text input
				$textInput = new TextInput();
				$textInput->setText($message);
				$textInput->setLanguageCode('en');
				
				//create query input
				$queryInput = new QueryInput();
				$queryInput->setText($textInput);
				
				//get response and relevant info
				$response = $sessionsClient->detectIntent($formattedSession, $queryInput);
				$result = $response->getQueryResult();
				$intent = $result->getIntent();
				$parameters = $result->getParameters();
				$pa = array();
				$param = $parameters->getFields();
						foreach ($param as $pa)
						{
							if($pa->getKind() == 'string_value')
							{
								$paravalue = $pa->getStringValue();
							}
						}
 
				if($intent->getDisplayName() == 'UserProvideOrderNumber')
				{
						//To save the order number into database before pass to the event
						if($data == $id)
						{
							$orderid = DB::table('sessions')->update(['order_id' => $paravalue]);
						}						
				}
				
				if($intent->getDisplayName() == 'Order - Status' || $intent->getDisplayName() == 'Order - TrackingNumber' || $intent->getDisplayName() == 'Order - ETA')
				{
					//To save the order number into database before pass to the event
						if($data == $id)
						{
							if($paravalue != NULL)
							{
								$orderid = DB::table('sessions')->update(['order_id' => $paravalue]);
							}
						}
				}
				
				if($intent->getDisplayName() == 'UserProvideProductCode')
				{
					//To save the product code into database before pass to the event
						if($data == $id)
						{
							$orderid = DB::table('sessions')->update(['product_name' => $paravalue]);
						}	
				}
				
				
				
				$ON2 = DB::table('sessions')->select('*')->get();
				foreach($ON2 as $ON1)
				{
					$ON = $ON1->order_id;
					$OFF = $ON1->product_name;
				}
				
				$stat = DB::table('order')->select('*')->where('order_id','=',$ON)->get();
								foreach($stat as $sta)
								{
									$oder = $sta->order_id;
									$stats = $sta->status;
									$ETA = $sta->eta;
									$track = $sta->tracking_number;
								}
								
				$products = DB::table('product')->select('*')->where('product_name','=',$OFF)->get();
								foreach($products as $product)
								{
									$price = $product->price;
									$quantity = $product->quantity;
									$delivery = $product->delivery_period;
									$shippingEM = $product->shipping_fee_em;
									$shippingWM = $product->shipping_fee_wm;
									$des = $product->description;
									$att = $product->attributes;
									$QandA = $product->product_QandA;
								}
				
					

				
				if($intent->getDisplayName() == 'Order - Status')
				{
						//set Event "Order_Status"
						$value = new Value();
						$value->setStringValue($stats);
						$struct = new Struct();
						$struct->setFields(['Order_Status' => $value]);
						
						$eventInput = new EventInput();
						$eventInput->setName('Order_Status');
						$eventInput->setLanguageCode('en');
						$eventInput->setParameters($struct);
						
						$queryParams->setPayload($struct);	
						
						$queryInput->setEvent($eventInput);
						$response = $sessionsClient->detectIntent($formattedSession, $queryInput,
						['queryParams' => $queryParams]);
						$result = $response->getQueryResult();
							
				}
				
				
				if($intent->getDisplayName() == 'Order - TrackingNumber')
				{
					//set Event "Order_Tracking"
						$value = new Value;
						$value->setStringValue($track);
						$struct = new Struct();
						$struct->setFields(['Order_Tracking' => $value]);
						
						$eventInput = new EventInput();
						$eventInput->setName('Order_Tracking');
						$eventInput->setLanguageCode('en');
						$eventInput->setParameters($struct);		
						
						$queryParams->setPayload($struct);	
					
						$queryInput->setEvent($eventInput);
						$response = $sessionsClient->detectIntent($formattedSession, $queryInput, 
						['queryParams' => $queryParams]);
						$result = $response->getQueryResult();
				}
				
				if($intent->getDisplayName() == 'Order - ETA')
				{
					//set Event "Order_ETA"
						$value = new Value;
						$value->setStringValue($ETA);
						$struct = new Struct();
						$struct->setFields(['Order_ETA' => $value]);
						
						$eventInput = new EventInput();
						$eventInput->setName('Order_ETA');
						$eventInput->setLanguageCode('en');
						$eventInput->setParameters($struct);		
						
						$queryParams->setPayload($struct);	
					
						$queryInput->setEvent($eventInput);
						$response = $sessionsClient->detectIntent($formattedSession, $queryInput, 
						['queryParams' => $queryParams]);
						$result = $response->getQueryResult();
				}
				
				if($intent->getDisplayName() == 'Product - Price')
				{
					//set Event "Product_Price"
						$value = new Value;
						$value->setStringValue($price);
						$struct = new Struct();
						$struct->setFields(['Product_Price' => $value]);
						
						$eventInput = new EventInput();
						$eventInput->setName('Product_Price');
						$eventInput->setLanguageCode('en');
						$eventInput->setParameters($struct);		
						
						$queryParams->setPayload($struct);	
					
						$queryInput->setEvent($eventInput);
						$response = $sessionsClient->detectIntent($formattedSession, $queryInput, 
						['queryParams' => $queryParams]);
						$result = $response->getQueryResult();
				}
				
				if($intent->getDisplayName() == 'Product - DeliveryPeriod')
				{
					//set Event "Product_DeliveryPeriod"
						$value = new Value;
						$value->setStringValue($delivery);
						$struct = new Struct();
						$struct->setFields(['Product_DeliveryPeriod' => $value]);
						
						$eventInput = new EventInput();
						$eventInput->setName('Product_DeliveryPeriod');
						$eventInput->setLanguageCode('en');
						$eventInput->setParameters($struct);		
						
						$queryParams->setPayload($struct);	
					
						$queryInput->setEvent($eventInput);
						$response = $sessionsClient->detectIntent($formattedSession, $queryInput, 
						['queryParams' => $queryParams]);
						$result = $response->getQueryResult();
				}
				
				if($intent->getDisplayName() == 'Product - ShippingFee')
				{
					//set Event "Product_ShippingFee"

						$value = new Value;
						$value->setStringValue($shippingEM);
						$value1 = new Value;
						$value1->setStringValue($shippingWM);
						$struct = new Struct();
						$struct->setFields(['Product_ShippingFeeEM' => $value,'Product_ShippingFeeWM' => $value1]);					
						
						$eventInput = new EventInput();
						$eventInput->setName('Product_ShippingFee');
						$eventInput->setLanguageCode('en');
						$eventInput->setParameters($struct);	
						
						$queryParams->setPayload($struct);	
					
						$queryInput->setEvent($eventInput);

						$response = $sessionsClient->detectIntent($formattedSession, $queryInput,
						['queryParams' => $queryParams]);
						$result = $response->getQueryResult();

				}
				
				if($intent->getDisplayName() == 'Product - Description')
				{
					//set Event "Product_Description"
						$value = new Value;
						$value->setStringValue($des);
						$struct = new Struct();
						$struct->setFields(['Product_Description' => $value]);
						
						$eventInput = new EventInput();
						$eventInput->setName('Product_Description');
						$eventInput->setLanguageCode('en');
						$eventInput->setParameters($struct);		
						
						$queryParams->setPayload($struct);	
					
						$queryInput->setEvent($eventInput);
						$response = $sessionsClient->detectIntent($formattedSession, $queryInput, 
						['queryParams' => $queryParams]);
						$result = $response->getQueryResult();
				}
				
				if($intent->getDisplayName() == 'Product - Attributes')
				{
					//set Event "Product_Attributes"
						$value = new Value;
						$value->setStringValue($att);
						$struct = new Struct();
						$struct->setFields(['Product_Attributes' => $value]);
						
						$eventInput = new EventInput();
						$eventInput->setName('Product_Attributes');
						$eventInput->setLanguageCode('en');
						$eventInput->setParameters($struct);		
						
						$queryParams->setPayload($struct);	
					
						$queryInput->setEvent($eventInput);
						$response = $sessionsClient->detectIntent($formattedSession, $queryInput, 
						['queryParams' => $queryParams]);
						$result = $response->getQueryResult();
				}
				
				if($intent->getDisplayName() == 'Product - Q&A')
				{
					//set Event "Product_QandA"
						$value = new Value;
						$value->setStringValue($QandA);
						$struct = new Struct();
						$struct->setFields(['Product_QandA' => $value]);
						
						$eventInput = new EventInput();
						$eventInput->setName('Product_QandA');
						$eventInput->setLanguageCode('en');
						$eventInput->setParameters($struct);		
						
						$queryParams->setPayload($struct);	
					
						$queryInput->setEvent($eventInput);
						$response = $sessionsClient->detectIntent($formattedSession, $queryInput, 
						['queryParams' => $queryParams]);
						$result = $response->getQueryResult();
				}
				
				if($intent->getDisplayName() == 'Product - Quantity')
				{
					//set Event "Product_Quantity"
						$value = new Value;
						$value->setStringValue($quantity);
						$struct = new Struct();
						$struct->setFields(['Product_Quantity' => $value]);
						
						$eventInput = new EventInput();
						$eventInput->setName('Product_Quantity');
						$eventInput->setLanguageCode('en');
						$eventInput->setParameters($struct);		
						
						$queryParams->setPayload($struct);	
					
						$queryInput->setEvent($eventInput);
						$response = $sessionsClient->detectIntent($formattedSession, $queryInput, 
						['queryParams' => $queryParams]);
						$result = $response->getQueryResult();
				}
				
				//Facebook message template
				$fulfillmentMessages = $result->getFulfillmentMessages();
				
				$res =  [
							'recipient' => ['id' => $id],
							'message' =>['text' => $result->getFulfillmentText()]
						];
						
				$this->sendMessage($res);
				
				if ($fulfillmentMessages) 
				{
					foreach ($fulfillmentMessages as $fulfillmentMessage) 
					{
						if($fulfillmentMessage->getPlatform() == Intent_Message_Platform::FACEBOOK)
						{	
							//Facebook quickreplies template
							if($FieldName = $fulfillmentMessage->getQuickReplies())
							{
								$title = $FieldName->getTitle();
								$quickRepliesMessage = $FieldName->getQuickReplies();
								$quickreply = array();
								foreach ($quickRepliesMessage as $msg) 
								{
										$quickreply[] = [
										'content_type' => 'text',
										'title' => $msg,
										'payload' => $msg
										];
								}
								$res = [
								'recipient' => ['id' => $id],
								'message' =>['text' => $title,
								'quick_replies' => $quickreply]
								];
								$this->sendMessage($res);
								break;
							}
							
							//Facebok Card template
							elseif($FieldName = $fulfillmentMessage->getCard())
							{
								$title = $FieldName->getTitle();
								$subtitle = $FieldName->getSubtitle();
								$image_uri = $FieldName->getImageURI();
								$buttons = $FieldName->getButtons();
								foreach($buttons as $button)
								{
									$text = $button->getText();
									$postback = $button->getPostback();
								}
									$res = [
										"recipient" => ["id" => $id],
										"message" => ["attachment" => ["type" => "template", "payload" => ['template_type' => "generic",
										"elements" =>[["title" => $title,
										"image_url" => $image_uri,
										"subtitle" => $subtitle,
										"default_action" => ["type" => "web_url",
										"url" => "www.youbeli.com",
										"webview_height_ratio" => "tall",],
										"buttons" => [[
										"type" => "web_url",
										"url" => $postback,
										"title" => $text],
										/*["type" => "postback",
										"title" => $text,
										"payload" =>$text]*/
										]]]]]]];
									$this->sendMessage($res);
										
							}
						
							//facebook image template
							elseif($FieldName = $fulfillmentMessage->getImage())
							{
									$image_uri = $FieldName->getImageURI();
						
									$res = [
										"recipient" => ["id" => $id],
										"message" => ["attachment" => ["type" => "image", "payload" => ["url" => $image_uri, "is_reusable" => true]]]];
									$this->sendMessage($res);
										
							}
						
							elseif($FieldName = $fulfillmentMessage->getText())
							{
								$texts = $FieldName->getText();
								foreach ($texts as $text);
								
								//To set what to reply order status is "complete"
								if(($intent->getDisplayName() == 'Order - Status') && ($stats == 'Complete'))
								{
									$res = [
												"recipient" => ["id" => $id],
												"message" => ["text" => $text]
												];
												$this->sendMessage($res);
								}
								//To set what to reply order status is "pending/imcomplete"
								elseif(($intent->getDisplayName() == 'Order - Status') && ($stats == 'Pending' || 'Incomplete'))
								{
									$res = [
												"recipient" => ["id" => $id],
												"message" => ["text" => $text."\n\nWe are yet to receive your payment,\"kindly make your payment to........\"
												\nIf you had made your payment, kindly upload your payment slip here. We will process your order within 24 working hours."]
												];
												$this->sendMessage($res);
								}
								//if order status is "pending/imcomplete"
								elseif(($intent->getDisplayName() == 'Order - TrackingNumber') && ($track == NULL))
								{
									$res = [
												"recipient" => ["id" => $id],
												"message" => ["text" => "There is no Tracking Number yet"]
												];
												$this->sendMessage($res);
								}
								elseif(($intent->getDisplayName() == 'Order - ETA') && ($ETA == NULL))
								{
									$res = [
												"recipient" => ["id" => $id],
												"message" => ["text" => "There is no Estimate Time of Arrival"]
												];
												$this->sendMessage($res);
								}
								else
								{
									$res = [
												"recipient" => ["id" => $id],
												"message" => ["text" => $text]
												];
												$this->sendMessage($res);
								}
								
										
							}
						}
					}
				}
				//dd($result);
				//update timestamp everytime once the user access messenger
				$database = DB::table('messenger')->where('sender_id','=',$id)->update(['last_receive' => $time]);
			}
		}
		
		else
		{
			exit();
		}
	}
	
	
	protected function sendMessage($res)
    {
        $ch = curl_init('https://graph.facebook.com/v2.6/me/messages?date_format=U&access_token=' . env('PAGE_ACCESS_TOKEN'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($res));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_exec($ch);
        curl_close($ch);
    }

	public function verifyToken()
	{
		$local_token = env('FACEBOOK_MESSENGER_WEBHOOK_TOKEN');
        $hub_verify_token = request('hub_verify_token');

        if($hub_verify_token === $local_token){
        echo request('hub_challenge');
        exit;
	}
	}
	
	protected function get_started($start)
    {
        $ch = curl_init('https://graph.facebook.com/v2.6/me/messenger_profile?access_token=' . env('PAGE_ACCESS_TOKEN'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($start));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/json']);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_exec($ch);
        curl_close($ch);

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}