<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Tgallice\FBMessenger\WebhookRequestHandler;

class MessengerController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$webookHandler = new WebhookRequestHandler('app_secret', 'verify_token');
	// The Request is internally retrieve but you can set your own if you have already a Request object.
	// $webookHandler = new WebhookRequestHandler('app_secret', 'verify_token', $request);


	if (!$webookHandler->isValidVerifyTokenRequest()) {
		echo "error";
	}

	// you must return a 200 OK HTTP response 
	header("HTTP/1.1 200 OK");

	echo $webookHandler->getChallenge();
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
